component {
	variables.defaultFormats = {
		"DATE"		: "yyyy-mm-dd",
		"DATETIME"	: "yyyy-mm-dd HH:nn:ss",
		"TIME"		: "hh:mm:ss",
		"TIMESTAMP"	: "yyyy-mm-dd hh:mm:ss"
	};
	
	Spreadsheet function init(String sheetName, Boolean useXMLFormat, String src, Numeric sheet){
		// If a sheet name was not provided, use the default "Sheet1"
		var sheetName = arguments.keyExists("sheetName")?arguments.sheetName:"Sheet1";
		
		if(arguments.keyExists("src") && arguments.keyExists("useXMLFormat")){
			throw(
				type	= "cfspreadsheet.spreadsheet",
				message	= "Invalid Argument Combination",
				detail	= "Cannot specify both 'src' and 'useXmlFormat'. Argument 'useXmlFormat' only applies to new spreadsheets");
		}
		
		arguments.keyExists("src")
			? loadExistingWorkbook(argumentCollection=arguments)
			: loadNewWorkbook(argumentCollection=arguments);
		
		return this;
	}
	
	private function loadNewWorkbook(){
		var sheetName = arguments.keyExists("sheetName") ? arguments.sheetName : "Sheet1";
		
		// Initialize our workbook with a blank Sheet
		variables.workbook = createWorkBook(argumentCollection=arguments);
		createSheet(sheetName=sheetName);
		setActiveSheet(sheetName=sheetName);
	}
	
	private function loadExistingWorkbook(){
		variables.workbook = loadFromFile(arguments.src);
		
		// activate the requested sheet	number
		if(arguments.keyExists("sheet")){
			validateSheetIndex(arguments.sheet);
			setActiveSheet(sheetIndex=arguments.sheet);
		}
		// activate sheet by name
		else if(arguments.keyExists("sheetname")){
			validateSheetName(arguments.sheetName);
			setActiveSheet(sheetName=arguments.sheetname);
		}
		// otherwise, activate the 1st sheet
		else {
			setActiveSheet(sheetIndex=1);
		}
	}
	
	private function createSheet(String sheetName, String nameConflict="error"){
		if(len(arguments.sheetName) gt 31){
			throw(
				type	= "cfspreadsheet.spreadsheet",
				message	= "Invalid Sheet Name",
				detail	= "The supplied sheet name is too long [#len(arguments.sheetName)#]. The maximum length is 31 characters.");
		}
		
		var newSheetName = arguments.keyExists("sheetName") ? arguments.sheetName : generateUniqueSheetName();
		
		// if this sheet is already in use...
		var sheetNum = workbook.getSheetIndex( javacast("string", newSheetName) ) + 1;
		// Workaround for POI bug that returns wrong index for "Sheet1" with empty workbooks
		if(sheetNum gt 0 && workbook.getNumberOfSheets() eq 0){
			sheetNum = 0;
		}
		
		if(sheetNum GT 0){
			// Replace the existing sheet
			if(arguments.nameConflict eq "overwrite"){
				deleteSheetAt(sheetNum);
			}
			else{
				throw(
					type	= "cfspreadsheet.spreadsheet",
					message	= "Invalid Sheet Name",
					detail	= "The Workbook already contains a sheet named [#arguments.sheetName#].");
			}
		}
		
		var newSheet = workbook.createSheet( javaCast("String", newSheetName) );
		
		// if overwriting, restore the sheet to its previous position
		if( sheetNum GT 0 and arguments.nameConflict EQ "overwrite" ){
			moveSheet( newSheetName, sheetNum );
		}
		
		return newSheet;
	}
	
	private function generateUniqueSheetName(){
		var startNum = workbook.getNumberOfSheets() + 1;
		var maxRetry = startNum + 250;
		var sheetNum = 0;
		
		for(sheetNum=startNum; sheetNum <= maxRetry; sheetNum++){
			var proposedName = "Sheet" & sheetNum;
			if( workbook.getSheetIndex( proposedName ) lt 0 ){
				return proposedName;
			}
		}
/*		loop(from="startNum", to="maxRetry", index="sheetNum"){
			var proposedName = "Sheet" & sheetNum;
			if( workbook.getSheetIndex( proposedName ) lt 0 ){
				return proposedName;
			}
		}*/
		
		// this should never happen. but if for some reason it did, warn the action failed and abort...
		throw(
			type	= "cfspreadsheet.spreadsheet",
			message	= "Unable to generate a unique sheet name",
			detail	= "Unable to generate a unique sheet name.");
		
	}
	
	private function deleteSheet(String sheetName, Numeric sheetIndex){
		var removeSheetNum = 0;
		validateSheetNameOrIndexWasProvided(argumentCollection=arguments);
		
		if(arguments.keyExists("sheetName")){
			validateSheetName(arguments.sheetName);
			removeSheetNum = workbook.getSheetIndex( sheetName ) + 1;
		}
		
		if(arguments.keyExists("sheetIndex")){
			validateSheetIndex(arguments.sheetIndex);
			removeSheetNum = arguments.sheetIndex;
		}
		
		// Do not allow all of the sheets to be deleted, or the component will not function properly
		if( workbook.getNumberOfSheets() lte 1 ){
			throw(
				type	= "cfspreadsheet.spreadsheet",
				message	= "Invalid Action",
				detail	= "Workbooks must always contain at least one sheet.");
		}
		
		deleteSheetAt(removeSheetNum);
		
	}
	
	private function deleteSheetAt(Numeric sheetIndex){
		return workbook.removeSheetAt( javaCast("int", arguments.sheetIndex - 1) );
	}
	
	private function moveSheet(required String sheetName, required Numeric sheet){
		// First make sure the sheet exists and the target position is within range --->
		validateSheetName( arguments.sheetName );
		validateSheetIndex( arguments.sheet );
		
		var moveToIndex = arguments.sheet - 1;
		workbook.setSheetOrder( javaCast("String", arguments.sheetName), javaCast("int", moveToIndex) );
	}
	
	private function createWorkBook(String sheetName="Sheet1", Boolean useXMLFormat=false){
		var class = arguments.useXMLFormat
			? "org.apache.poi.xssf.usermodel.XSSFWorkbook"
			: "org.apache.poi.hssf.usermodel.HSSFWorkbook";
			
		return loadPOI(class).init();
	}
	
	private function loadFromFile(required String src){
		var input = createObject("java", "java.io.FileInputStream").init(arguments.src);
		var buffered = createObject("java", "java.io.BufferedInputStream").init(input);
		var workbookFactory	= loadPoi("org.apache.poi.ss.usermodel.WorkbookFactory");
		var workbook = workbookFactory.create(buffered);
		input.close();
		buffered.close();
		
		return workbook;
	}
	
	private function sheetExists(String sheetName, Numeric sheetIndex){
		validateSheetNameOrIndexWasProvided(argumentCollection=arguments);
		var sheetIDX = getSheetIndex(argumentCollection=arguments);
		
		// the position is valid if it an integer between 1 and the total number of sheets in the workbook
		 return ( sheetIDX GT 0 && sheetIDX EQ round(sheetIDX) && sheetIDX LTE workbook.getNumberOfSheets() );
	}
	
	private function setActiveSheet(String sheetName, Numeric sheetIndex){
		validateSheetNameOrIndexWasProvided(argumentCollection=arguments);
		
		if(arguments.keyExists("sheetName")){
			validateSheetName(arguments.sheetName);
		}
		
		var sheetIDX = getSheetIndex(argumentCollection=arguments);
		
		validateSheetIndex(sheetIDX);
		workbook.setActiveSheet(javaCast("int", sheetIDX - 1));
	}
	
	private function getSheetIndex(){
		return arguments.keyExists("sheetIndex")
			? arguments.sheetIndex
			: workbook.getSheetIndex( javaCast("string", arguments.sheetName) ) + 1;
	}
	
	private function validateSheetNameOrIndexWasProvided(String sheetName, Numeric sheetIndex){
		if( arguments.keyExists("sheetName") && arguments.keyExists("sheetIndex") ){
			throw(
				type	= "cfspreadsheet.spreadsheet",
				message	= "Cannot Provide Both Sheet and SheetName Attributes",
				detail	= "Only one of either 'sheet' or 'sheetname' attributes may be provided.");
		}
		if( !arguments.keyExists("sheetName") && !arguments.keyExists("sheetIndex") ){
			throw(
				type	= "cfspreadsheet.spreadsheet",
				message	= "Missing Required Argument",
				detail	= "Either 'sheet' or 'sheetname' attributes must be provided.");
		}
	}
	
	private function validateSheetName(String sheetName){
		if( !sheetExists(sheetName=arguments.sheetName) ){
			throw(
				type	= "cfspreadsheet.spreadsheet",
				message	= "Invalid Sheet Name [#arguments.sheetName#]",
				detail	= "The requested sheet was not found in the current workbook.");
		}
	}
	
	private function validateSheetIndex(required Numeric sheetIndex){
		if( !sheetExists(sheetIndex=arguments.sheetIndex) ){
			var sheetCount = workbook.getNumberOfSheets();
			throw(
				type	= "cfspreadsheet.spreadsheet",
				message	= "Invalid Sheet Index [#arguments.sheetIndex#]",
				detail	= "The SheetIndex must a whole number between 1 and the total number of sheets in the workbook [#Local.sheetCount#].");
		}
	}
	
	private function loadPOI(required String javaclass){
		//return createObject("java", javaclass, "cfspreadsheet", "3.0.1");
		//return createObject("java", javaclass, "org.apache.servicemix.bundles.poi", "4.0.1.3");
		return createObject("java", javaclass);
	}
	
	
}